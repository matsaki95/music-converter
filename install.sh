#!/usr/bin/env bash

echo 'Making sure ~/bin exists...'
mkdir -p ~/bin
exitcode=${?}
if [[ ${exitcode} != 0 ]]; then
    echo 'Failed!'
    exit ${exitcode}
fi

echo 'Moving executable to ~/bin...'
mv music-converter ~/bin
exitcode=${?}
if [[ ${exitcode} != 0 ]]; then
    echo 'Failed!'
    exit ${exitcode}
fi

echo 'Done!'
echo 'As a final step, make sure that the file ~/.profile contains the following line:'
echo 'PATH:"$HOME/bin:$HOME/.local/bin:$PATH"'
