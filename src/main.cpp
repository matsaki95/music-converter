#include <iostream>
#include <boost/filesystem.hpp>
#include "ProgramOptions/ProgramOptions.h"
#include "Node.h"
#include "readContents.h"
#include "runFFMPEG.h"
#include "findSolution.h"
#include "cleanUp.h"

int main(int argc, char* argv[])
{
    programOptions.parseProgramOptions(argc, argv);

    rootNodes.source.contents = readContents(rootNodes.source.path, "source", true);
    if (boost::filesystem::exists(rootNodes.destination.path))//It's not necessary for the destination to exist yet
        rootNodes.destination.contents = readContents(rootNodes.destination.path, "destination");

    HashVector oldHashes(rootNodes.destination.path.string()+"/.music-converter-cache");
    HashVector newHashes(rootNodes.source);

    Node nodeToConvert;//Basically a queue of files to convert
    nodeToConvert.path = rootNodes.source.path;
    nodeToConvert.isAFolder = true;
    std::queue<std::string> itemsToRemove;
    findSolution(nodeToConvert, itemsToRemove, newHashes, oldHashes);
    cleanUp(itemsToRemove);

    runFFMPEG(nodeToConvert, rootNodes.destination.path.string());
    newHashes.save(rootNodes.destination.path.string()+"/.music-converter-cache");

    return 0;
}
