#ifndef MUSIC_CONVERTER_HASH_H
#define MUSIC_CONVERTER_HASH_H

#include <string>

struct Hash {
    std::string path;
    std::string hash;
};

bool hashCompare(const Hash& a, const Hash& b)
{
    return a.path<b.path;
}

#endif //MUSIC_CONVERTER_HASH_H
