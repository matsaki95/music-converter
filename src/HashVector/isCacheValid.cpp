#include "HashVector.h"

std::string getline(std::ifstream& input)
{
    std::string buffer;
    std::getline(input, buffer);
    return buffer;
}

bool HashVector::isCacheValid(std::ifstream& cache)
{
    if (!cache.good())
        return false;
    try {
        switch (std::stoi(getline(cache))) {//The first line is the version number
        case 2:
            if (getline(cache)!=programOptions.bitrate)
                return false;
            if (getline(cache)!=programOptions.outputExtension)
                return false;
            return (getline(cache)==programOptions.ffmpegLibrary);
        case 1:
            if (programOptions.bitrate!="320k")
                return false;
            if (programOptions.outputExtension!=".ogg")
                return false;
            return (programOptions.ffmpegLibrary=="libvorbis");
        default:
            return false;
        }
    }
    catch (const std::invalid_argument& exception) {//This will be thrown if the first line of the file isn't an integer
        return false;
    }
}
