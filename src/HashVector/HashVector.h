#ifndef MUSIC_CONVERTER_HASHVECTOR_H
#define MUSIC_CONVERTER_HASHVECTOR_H
#define CURRENT_CACHE_VERSION 2

#include <fstream>
#include <vector>
#include <queue>
#include <algorithm>
#include <thread>
#include <mutex>
#include <atomic>
#include "Hash.h"
#include "../ProgramOptions/ProgramOptions.h"
#include "../generateFileQueue.h"

class HashVector {
private:
    std::vector<Hash> hashes;
    bool sorted = true;
    std::mutex fileQueueMutex, hashesMutex;

    void sort()
    {
        std::sort(hashes.begin(), hashes.end(), hashCompare);
        sorted = true;
    }

    bool isCacheValid(std::ifstream& cache);

    void writeMetadata(std::ofstream& output)
    {
        output << CURRENT_CACHE_VERSION << '\n'
               << programOptions.bitrate << '\n'
               << programOptions.outputExtension << '\n'
               << programOptions.ffmpegLibrary << '\n';
    }

    std::string generateHash(const std::string& inputPath);

    void generateHashes(std::queue<std::string>& fileQueue, std::atomic<unsigned>& currentFile, const unsigned totalFiles);

public:
    explicit HashVector(const Node& node)
    {
        if (programOptions.verbosity>0)
            std::cout << "Generating new hashes..." << std::flush;
        std::queue<std::string> fileQueue;
        generateFileQueue(node, fileQueue);
        long unsigned totalFiles = fileQueue.size();
        std::atomic<unsigned> currentFile;
        currentFile = 0;
        unsigned threadsAmount = std::thread::hardware_concurrency();
        if (threadsAmount==0)
            threadsAmount = 1;
        std::thread threads[threadsAmount];
        for (auto& th : threads)
            th = std::thread(&HashVector::generateHashes, this, std::ref(fileQueue), std::ref(currentFile), totalFiles);
        for (auto& th : threads)
            th.join();
        if (programOptions.verbosity>0)
            std::cout << "\r\e[JGenerated " << currentFile << " new hashes" << std::endl;
    }

    explicit HashVector(const std::string& cacheLocation)
    {
        if (programOptions.verbosity>0)
            std::cout << "Loading old hashes from cache..." << std::flush;
        std::ifstream inputFile(cacheLocation);
        if (isCacheValid(inputFile)) {
            while (!inputFile.eof()) {
                std::string buffer;
                std::getline(inputFile, buffer);//First line is the path
                if (buffer.empty())
                    continue;//Means we must have reached the last (empty but for a newline) line of the file
                hashes.emplace_back();
                hashes.back().path = buffer;
                std::getline(inputFile, hashes.back().hash);//Second line is the hash
                if (programOptions.verbosity>0)
                    std::cout << "\r\e[JLoading " << hashes.size() << " old hashes from cache..." << std::flush;
            }
        }
        sorted = false;
        if (programOptions.verbosity>0)
            std::cout << "\r\e[JLoaded " << hashes.size() << " old hashes from cache" << std::endl;
    }

    void add(const std::string& path, const std::string& hash)
    {
        hashes.emplace_back();
        hashes.back().path = path;
        hashes.back().hash = hash;
        sorted = false;
    }

    std::string getHash(const std::string& path)
    {
        if (!sorted)
            sort();
        Hash dummy;
        dummy.path = path;
        auto pointer = std::lower_bound(hashes.begin(), hashes.end(), dummy, hashCompare);
        if (pointer!=hashes.end())
            if (pointer->path==path)
                return pointer->hash;
        return "";
    }

    void save(const std::string& destination)
    {
        if (programOptions.verbosity>0)
            std::cout << "\r\e[JSaving hashes to cache..." << std::flush;
        if (!programOptions.simulationMode) {
            std::ofstream outputFile(destination);
            writeMetadata(outputFile);
            for (unsigned i = 0; i<hashes.size(); ++i) {
                if (programOptions.verbosity>0)
                    std::cout << "\r\e[JSaving " << i+1 << " hashes to cache..." << std::flush;
                outputFile << hashes[i].path << '\n' << hashes[i].hash << '\n';
            }
        }
        if (programOptions.verbosity>0)
            std::cout << "\r\e[JSaved " << hashes.size() << " hashes to cache" << std::endl;
    }
};

#include "generateHashes.cpp"
#include "isCacheValid.cpp"

#endif //MUSIC_CONVERTER_HASHVECTOR_H
