#ifndef MUSIC_CONVERTER_FOLDER_H
#define MUSIC_CONVERTER_FOLDER_H

#include <string>
#include <vector>
#include <boost/filesystem.hpp>

struct Node {
    boost::filesystem::path path;
    bool isAFolder = false;
    std::vector<Node> contents;//If it's a folder, these are the contents
};

bool nodeCompare(const Node& a, const Node& b)
{
    return a.path<b.path;
}

#endif //MUSIC_CONVERTER_FOLDER_H
