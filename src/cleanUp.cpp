#include <queue>
#include "ProgramOptions/ProgramOptions.h"

void cleanUp(std::queue<std::string>& itemsToRemove)
{
    unsigned long totalItems = itemsToRemove.size();
    for (unsigned long i = 0; i<totalItems; ++i) {
        std::string item = itemsToRemove.front();
        itemsToRemove.pop();
        std::string command = "rm -r \""+item+"\"";
        if (programOptions.verbosity>0)
            std::cout << "\r\e[J[" << i+1 << '/' << totalItems << "] Cleaning up " << boost::filesystem::basename(item) << "..." << std::flush;
        if (!programOptions.simulationMode)
            system(command.c_str());
    }
    if (programOptions.verbosity>0)
        std::cout << "\r\e[JCleaned up " << totalItems << " items" << std::endl;
}
