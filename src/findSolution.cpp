#include <vector>
#include "Node.h"
#include "HashVector/HashVector.h"

void findSolution(Node& source, Node& destination, Node& nodeToConvert, std::queue<std::string>& itemsToRemove, HashVector& newHashes, HashVector& oldHashes)
{
    //Sorting: easing mass data processing since the dawn of computers (comparing is easier/faster this way)
    std::sort(source.contents.begin(), source.contents.end(), nodeCompare);
    std::sort(destination.contents.begin(), destination.contents.end(), nodeCompare);

    unsigned destinationIndex = 0;
    for (auto& sourceItem : source.contents) {
        if (destinationIndex==destination.contents.size()) {
            nodeToConvert.contents.push_back(sourceItem);
            continue;
        }
        while (basename(destination.contents[destinationIndex].path)<basename(sourceItem.path)) {//A higher order against the destination means the destination got removed
            itemsToRemove.push(destination.contents[destinationIndex].path.string());
            ++destinationIndex;
            if (destinationIndex==destination.contents.size()) {//We may run out of bounds
                nodeToConvert.contents.push_back(sourceItem);
                break;//Break out of the while loop
            }
        }
        if (destinationIndex==destination.contents.size())//Testing if we broke out of the while loop above
            continue;
        if (basename(destination.contents[destinationIndex].path)>basename(sourceItem.path)) {//A lower order against the destination means the path is new
            nodeToConvert.contents.push_back(sourceItem);
            continue;//Not increasing destinationIndex because this file may share it's name with the next
        }
        //If the destination item ranks neither lower nor higher, it means it's name is the same as the source's... Which means checking hashes or stepping into the folder.
        if (sourceItem.isAFolder) {
            if (!destination.contents[destinationIndex].isAFolder) {//They should match, otherwise things have changed radically
                nodeToConvert.contents.push_back(sourceItem);
                itemsToRemove.push(destination.contents[destinationIndex].path.string());
                ++destinationIndex;
                continue;
            }
            nodeToConvert.contents.emplace_back();
            nodeToConvert.contents.back().path = sourceItem.path;
            nodeToConvert.contents.back().isAFolder = true;
            findSolution(sourceItem, destination.contents[destinationIndex], nodeToConvert.contents.back(), itemsToRemove, newHashes, oldHashes);
            if (nodeToConvert.contents.back().contents.empty())//It's possible that there was nothing to convert in the folder
                nodeToConvert.contents.pop_back();//In that case, we remove the folder from the queue
            ++destinationIndex;
        }
        else {
            if (destination.contents[destinationIndex].isAFolder) {//Again, if the source is a file, so should be the destination
                nodeToConvert.contents.push_back(sourceItem);
                itemsToRemove.push(destination.contents[destinationIndex].path.string());
                ++destinationIndex;
                continue;
            }
            //Now we need to make sure only the correct extension exists and also see if there is a need of conversion
            bool foundProperExtension = false;
            while (basename(destination.contents[destinationIndex].path)==basename(sourceItem.path)) {
                if (destination.contents[destinationIndex].path.extension()==programOptions.outputExtension) {
                    foundProperExtension = true;
                    if (newHashes.getHash(sourceItem.path.string())!=oldHashes.getHash(sourceItem.path.string())) {//There should always be a new hash so even if there isn't an old one, they still don't match and the file is treated as changed
                        nodeToConvert.contents.push_back(sourceItem);
                    }
                }
                else {
                    itemsToRemove.push(destination.contents[destinationIndex].path.string());
                }
                ++destinationIndex;
                if (destinationIndex==destination.contents.size()) {//We may get to the end of the folder
                    break;//If so, stop checking extensions; there aren't any left
                }
            }
            if (!foundProperExtension) {
                nodeToConvert.contents.push_back(sourceItem);
            }
        }
    }
    while (destinationIndex<destination.contents.size()) {
        //I knew something was missing from this algorithm since I wrote it, but I couldn't put my finger on it
        //Finally realised that it will leave out items in the destination if they rank higher than the last source item
        itemsToRemove.push(destination.contents[destinationIndex].path.string());
        ++destinationIndex;
    }
}

void findSolution(Node& nodeToConvert, std::queue<std::string>& itemsToRemove, HashVector& newHashes, HashVector& oldHashes)
{
    if (programOptions.verbosity>0)
        std::cout << "Finding differences..." << std::flush;
    findSolution(rootNodes.source, rootNodes.destination, nodeToConvert, itemsToRemove, newHashes, oldHashes);
    if (programOptions.verbosity>0)
        std::cout << "\r\e[JFound solution for quick conversion" << std::endl;
}
