#include <algorithm>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
#include "ProgramOptions/ProgramOptions.h"
#include "RootNodes.h"
#include "Node.h"

using namespace boost::filesystem;//Because FML

std::vector<Node> readContents(const path& folder, unsigned& fileCount, bool useFilters)
{
    if (programOptions.verbosity>0)
        std::cout << "\r\e[JReading into " << folder.string() << "..." << std::flush;
    std::vector<Node> contents;
    std::vector<path> pathContents;
    copy(directory_iterator(folder), directory_iterator(), back_inserter(pathContents));

    for (const auto& pathItem : pathContents) {
        if (useFilters && programOptions.checkFilename(pathItem.filename().string()))
            continue;
        if (useFilters && !is_directory(pathItem) && !programOptions.checkExtension(pathItem.extension().string()))
            continue;
        contents.emplace_back();
        contents.back().path = absolute(pathItem);
        if (is_directory(pathItem)) {
            contents.back().isAFolder = true;
            contents.back().contents = readContents(pathItem, fileCount, useFilters);
        }
        else {
            ++fileCount;
            if (programOptions.verbosity>0)
                std::cout << "\r\e[JReading " << fileCount << " files..." << std::flush;
        }
        if (contents.back().isAFolder && contents.back().contents.empty())
            contents.pop_back();//No meaning in empty folders
    }

    return contents;
}

std::vector<Node> readContents(const path& folder, const std::string& identifier, bool useFilters = false)
{
    unsigned fileCount = 0;
    std::vector<Node> contents = readContents(folder, fileCount, useFilters);

    if (programOptions.verbosity>0)
        std::cout << "\r\e[JRead " << fileCount << " files at the " << identifier << std::endl;
    return contents;
}
