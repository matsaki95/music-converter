#include <queue>
#include "Node.h"

void generateFileQueue(const Node& node, std::queue<std::string>& fileQueue)
{
    for (auto& item : node.contents) {
        if (item.isAFolder) {
            generateFileQueue(item, fileQueue);
        }
        else {
            fileQueue.push(item.path.string());
        }
    }
}
