#ifndef MUSIC_CONVERTER_PROGRAMOPTIONS_H
#define MUSIC_CONVERTER_PROGRAMOPTIONS_H

#include <string>
#include <getopt.h>
#include <boost/filesystem.hpp>

class ProgramOptions {
private:
    std::string source, destination;
    std::vector<std::string> filenamesToExclude;
    std::vector<std::string> inputExtensions;
public:
    std::string programCall;
    unsigned verbosity = 1;
    std::string bitrate = "320k";
    std::string outputExtension = ".ogg";
    std::string ffmpegLibrary = "libvorbis";
    bool simulationMode = false;

    void parseProgramOptions(int argc, char* argv[]);

    bool checkFilename(const std::string& value)
    {
        return std::binary_search(filenamesToExclude.begin(), filenamesToExclude.end(), value);
    }

    bool checkExtension(const std::string& value)
    {
        return (inputExtensions.empty() || std::binary_search(inputExtensions.begin(), inputExtensions.end(), value));
    }
} programOptions;

const char* shortOptions = "he:b:i:o:l:vqs";

const option longOptions[] = {
        {"help",             no_argument,       nullptr, 'h'},
        {"exclude",          required_argument, nullptr, 'e'},
        {"bitrate",          required_argument, nullptr, 'b'},
        {"input-extension",  required_argument, nullptr, 'i'},
        {"output-extension", required_argument, nullptr, 'o'},
        {"ffmpeg-library",   required_argument, nullptr, 'l'},
        {"verbose",          no_argument,       nullptr, 'v'},
        {"quiet",            no_argument,       nullptr, 'q'},
        {"simulation",       no_argument,       nullptr, 's'},
        {nullptr,            no_argument,       nullptr, 0}
};

#include "parseProgramOptions.cpp"

#endif //MUSIC_CONVERTER_PROGRAMOPTIONS_H
