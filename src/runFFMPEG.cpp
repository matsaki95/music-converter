#include <vector>
#include <string>
#include <boost/filesystem.hpp>
#include "ProgramOptions/ProgramOptions.h"
#include "RootNodes.h"
#include "Node.h"
#include "countFiles.h"

void runFFMPEG(const Node& source, const std::string& destination, unsigned& currentFile, const unsigned fileCount)
{
    if (programOptions.verbosity>0)
        std::cout << "\r\e[JCreating directory " << destination << "..." << std::flush;
    if (!programOptions.simulationMode)
        boost::filesystem::create_directories(destination);

    for (const auto& content : source.contents) {
        if (content.isAFolder) {
            runFFMPEG(content, destination+"/"+content.path.filename().string(), currentFile, fileCount);
            continue;
        }
        ++currentFile;
        if (programOptions.verbosity>0)
            std::cout << "\r\e[J[" << currentFile << "/" << fileCount << "] Converting " << boost::filesystem::basename(content.path) << "..." << std::flush;
        if (programOptions.verbosity>1)
            std::cout << std::endl;//So that ffmpeg outputs on the next line

        std::string command = "ffmpeg -y -i \""+content.path.string()+"\"";
        command += " -codec:a "+programOptions.ffmpegLibrary;
        command += " -b:a "+programOptions.bitrate;
        if (programOptions.verbosity<2)
            command += " -loglevel warning";
        command += " \""+destination+"/"+basename(content.path)+programOptions.outputExtension+"\"";
        if (!programOptions.simulationMode) {
            system(command.c_str());
        }
    }
}

void runFFMPEG(const Node& source, const std::string& destination)
{
    unsigned currentFile = 0;
    unsigned fileCount = countFiles(source);
    runFFMPEG(source, destination, currentFile, fileCount);
    if (programOptions.verbosity>0)
        std::cout << "\r\e[JConverted " << currentFile << " files" << std::endl;
}
