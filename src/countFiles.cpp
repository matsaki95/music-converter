#include "Node.h"

unsigned countFiles(const Node& folder)
{
    unsigned count = 0;
    for (const auto& item : folder.contents) {
        if (item.isAFolder)
            count += countFiles(item);
        else
            ++count;
    }
    return count;
}
