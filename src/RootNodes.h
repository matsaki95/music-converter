#ifndef MUSIC_CONVERTER_ROOTNODES_H
#define MUSIC_CONVERTER_ROOTNODES_H

#include "Node.h"

struct RootNodes {
    Node source;
    Node destination;
} rootNodes;

#endif //MUSIC_CONVERTER_ROOTNODES_H
