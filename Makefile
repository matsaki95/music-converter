CXX=g++
CXXFLAGS=-Wall -O2

SRC=$(shell find src/ -path "*.cpp")
SRC+=$(shell find src/ -path "*.h")

all: music-converter

music-converter: $(SRC)
	$(CXX) $(CXXFLAGS) -o $@ src/main.cpp -pthread -lboost_filesystem -lboost_system -lcrypto++

install: music-converter
	./install.sh
